/**
 * 
 */
package com.noname.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.noname.model.Greeting;

/**
 * @author Artas
 *
 */
public class GreetingServiceTest {

  private GreetingService greetingService;
  
  @Before
  public void setUp() {
    this.greetingService = new GreetingService();
  }

  @Test
  public void testDefaultGreeting() {
    final String defaultMessage = "Hello world !";
    Greeting greeting = greetingService.createGreeting();
    
    assertEquals(greeting.getMessage(), defaultMessage);
  }
  
  @Test
  public void testSpecificGreeting() {
    final String name = "John";
    final String expectedGreeting = "Hello " + name + " !";
    Greeting greeting = greetingService.createGreeting(name);
    
    assertEquals(greeting.getMessage(), expectedGreeting);
  }

}
