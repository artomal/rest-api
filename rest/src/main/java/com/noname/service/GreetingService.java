/**
 * 
 */
package com.noname.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.noname.model.Greeting;

/**
 * @author Artas
 *
 */
@Service
public class GreetingService {
  
  private static final String DEFAULT_GREETING = "Hello world !";
  private static final String GREETING_WORD = "Hello ";
  
  /**
   * Creates default greeting
   * 
   * @return
   */
  public Greeting createGreeting(){
    Greeting greeting = new Greeting(DEFAULT_GREETING);
    return greeting;
  }
  
  /**
   * Creates specific greeting
   * 
   * @param name - recipient name
   * @return
   */
  public Greeting createGreeting(String name){
    String message = DEFAULT_GREETING;
    if(name != null && !StringUtils.isEmpty(name)){
      message = GREETING_WORD + name + " !";
    }
    Greeting greeting = new Greeting(message);
    
    return greeting;
  }
  
}
