/**
 * 
 */
package com.noname.service;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import com.noname.exception.NoFileFoundException;

/**
 * @author Artas
 *
 */
@Service
public class ResourceService {

  /**
   * Retrieves file content from resources
   * 
   * @param path - resource location
   * @return
   */
  public byte[] getFile(String path){
    byte[] content = null;
    Resource resource = new ClassPathResource(path);
    try {
      InputStream stream = resource.getInputStream();
      content = IOUtils.toByteArray(stream);
    } catch (IOException e) {
      throw new NoFileFoundException("File doesn't exist");
    }
    return content;
  }
  
}
