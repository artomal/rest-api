/**
 * 
 */
package com.noname.exception;

/**
 * @author Artas
 *
 */
public class ErrorResponse {
  
  private ErrorResponseBody error;
  
  public ErrorResponse(int status, String message){
    this.error = new ErrorResponseBody(status, message);
  }

  public ErrorResponseBody getError() {
    return error;
  }

  public void setError(ErrorResponseBody error) {
    this.error = error;
  }
  
}
