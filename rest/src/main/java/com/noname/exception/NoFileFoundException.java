/**
 * 
 */
package com.noname.exception;

/**
 * @author Artas
 *
 */
public class NoFileFoundException extends RuntimeException {
  
  private static final long serialVersionUID = 1L;

  public NoFileFoundException(String message) {
    super(message);
  }

}
