/**
 * 
 */
package com.noname.exception;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author Artas
 *
 */
@ControllerAdvice
public class CustomExceptionHandler {
  
  private static final Logger log = Logger.getLogger( CustomExceptionHandler.class.getName() );

  @ExceptionHandler(NoHandlerFoundException.class)
  @ResponseStatus(value=HttpStatus.NOT_FOUND)
  public @ResponseBody ErrorResponse exception(HttpServletRequest req, Exception ex) {
    return new ErrorResponse(404, "Endpoint doesn't exist");
  }
  
  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(value=HttpStatus.BAD_REQUEST)
  public @ResponseBody ErrorResponse typeException(Exception ex) {
    return new ErrorResponse(400, "Incorrect request syntax");
  }
  
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  @ResponseStatus(value=HttpStatus.METHOD_NOT_ALLOWED)
  public @ResponseBody ErrorResponse methodException(Exception ex) {
    return new ErrorResponse(405, "Method not supported");
  }
  
  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
  public void runtimeException(Exception ex) {
    log.log(Level.SEVERE, ex.toString(), ex);
  }

}
