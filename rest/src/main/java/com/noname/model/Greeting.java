/**
 * 
 */
package com.noname.model;

/**
 * @author Artas
 *
 */
public class Greeting {
  
  private String message;
  
  public Greeting(String name){
    this.message = name;
  }

  public String getMessage() {
    return message;
  }
  
}
