/**
 * 
 */
package com.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.noname.model.Greeting;
import com.noname.service.GreetingService;

/**
 * @author Artas
 *
 */
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class Hello {

  @Autowired
  private GreetingService greetingService;

  @RequestMapping(value = "/hello", method = RequestMethod.POST)
  public Greeting getHello(@RequestParam(value="name", required = false) String name) {
    Greeting greeting = greetingService.createGreeting(name);
    return greeting;
  }

}
