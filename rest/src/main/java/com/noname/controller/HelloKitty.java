/**
 * 
 */
package com.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.noname.service.ResourceService;

/**
 * @author Artas
 *
 */
@RestController
public class HelloKitty {
  
  private static final String IMAGE_PATH = "/images/hellokitty.jpg";
  
  @Autowired
  private ResourceService resourceService;

  @RequestMapping(value = "/hellokitty", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
  public byte[] getHelloKitty() {
    return resourceService.getFile(IMAGE_PATH);
  }

}
